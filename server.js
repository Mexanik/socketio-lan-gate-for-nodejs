﻿// Подключаем модуль и ставим на прослушивание 666 порта - связь с веб интерфейсом
var io = require('socket.io').listen(666); 
var dgram = require("dgram");
var buf;
var express = require('express');
var app = express();
var server = dgram.createSocket("udp4");

//отсылаем то что пришло из сервера в веб-интерфейс
server.on("message", function (msg, rinfo) {
  	console.log("from SH: "+msg.toString());
	io.sockets.emit('messagefromSH', msg.toString());
	//console.log("---");
});
//на этом порту слушаем UDP от Qt сервера
server.bind(6668);

// Отключаем вывод полного лога - пригодится в production'е
io.set('log level', 1);

//то, что пришло от веб-интерфейса посылаем серверу на 6669 порт
// Навешиваем обработчик на подключение нового клиента
io.sockets.on('connection', function (socket) {
	socket.on('messagefromCl', function (msg) {
		console.log("from WEB: "+msg);
		buf = Buffer(msg);
		server.send(buf, 0, buf.length, 6669, "localhost");
	});
});

//туда же посылаем команды от часов
//типа REST API
app.get('/send/:id', function(req, res){
	buf = Buffer(req.params.id);
	console.log("from REST: "+buf.toString());
	server.send(buf, 0, buf.length, 6669, "localhost");
});
// а оно на 3000 порту
app.listen(3000);